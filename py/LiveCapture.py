import pyshark

# g_interface = 'wlx10bef55eccbe'
# g_interface = 'ens33'
g_interface = 'any'
g_display_filter = 'ssl'
g_timeout = 15.0
g_textfile = './%livecap.txt'

def life_capture(interface='any', display_filter='', timeout=10, output_file='./live_cap.txt'):
    capture = pyshark.LiveCapture(interface=interface, display_filter=display_filter)
    capture.sniff(timeout=timeout)
    str_sep = '******************************\n'
    str_header = "interface='" + interface + "'\ndisplay_filter='" + display_filter + \
                 "'\ntimeout='" + str(timeout) + "'\ntextfile='" + output_file + "'\n" + str_sep
    str_capture = str()
    i = 0
    while True:
        try:
            i += 1
            str_capture += (str(i) + '\n' + str(capture.next()) + '\n' + str_sep)
        except StopIteration:
            print('total length = ', i - 1)
            break
    f = open(file=output_file, mode='wt', encoding='utf-8')
    f.write(str_header + str_capture)
    f.close()

life_capture(interface=g_interface, display_filter=g_display_filter, timeout=g_timeout, output_file=g_textfile)
