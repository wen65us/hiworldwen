import pyshark
from os import listdir
from os.path import join, isfile

# g_filter_youtube = "gquic" -T fields -e gquic.tag.uaid #1.youtube
g_filter_gmail = 'ssl.handshake.extensions_server_name contains "google.com"'  # "ssl" -T fields -e ssl.handshake.extensions_server_name
# g_filter_googlemap = "gquic" -T fields -e gquic.tag.uaid
# g_filter_samsung_pay = "http" -T fields -e http.request.uri.query -e urlencoded-form.value -e http.location

# g_display_filter = 'ssl.handshake.extensions_server_name'
g_display_filter = 'ssl'
g_pcapfolder = './litcp'
# g_pcapfolder = './pcaps'
g_textfile_prefix = './%filecap'


def list_only_files(pcap_folder):
    return [objfile for objfile in listdir(pcap_folder) if isfile(join(pcap_folder, objfile))]


def file_capture(pcap_folder='./pcap', display_filter='', output_file_prefix='./file_cap'):
    pcap_filelist = list_only_files(pcap_folder=g_pcapfolder)
    for pcapfile in pcap_filelist:
        capture = pyshark.FileCapture(input_file=join(pcap_folder, pcapfile),
                                      display_filter=display_filter)
        str_sep = '******************************\n'
        str_header = "pcap_file = '" + pcap_folder + '/' + pcapfile + \
                     "'\ndisplay_filter = '" + display_filter + "'"
        i = 0
        print(str_header)
        filename = output_file_prefix + '-' + pcapfile + '.txt'
        print("open file : " + filename)
        f = open(file=filename, mode='wt', encoding='utf-8')
        f.write(str_header + '\n' + str_sep)
        while True:
            try:
                i += 1
                newcapture = str(capture.next())
                f.write(str(i) + '\n' + newcapture + str_sep)
            except StopIteration:
                print('total packets : ', i - 1)
                break
        f.close()
        print("file saved.\n")

if __name__ == '__main__':
    file_capture(pcap_folder=g_pcapfolder, display_filter=g_display_filter, output_file_prefix=g_textfile_prefix)
    print('done')
