import pyshark, time


def listen_on_interface(interface, timeout=20):
    '''
    :param interface:
    :param timeout:
    :return:
    '''
    print('in')
    start = time.time()
    capture = pyshark.LiveCapture(interface=interface)
    print(capture)

    for item in capture.sniff_continuously():
        if timeout and time.time() - start > timeout:
            break
        yield item
    print('out')
    return capture


myinterface = 'any'
x = listen_on_interface(interface=myinterface)
print(x)
